﻿/// <binding BeforeBuild='clean' AfterBuild='min' Clean='clean' />

var gulp = require("gulp"),
    rimraf = require("rimraf"),
    concat = require("gulp-concat"),
    cssmin = require("gulp-cssmin"),
    uglify = require("gulp-uglify");

var gzip = require('gulp-gzip');
var paths = {
    webroot: "./wwwroot/"
};

paths.js = paths.webroot + "js/**/*.js";
paths.minJs = paths.webroot + "js/**/*.min.js";
paths.css = paths.webroot + "css/**/*.css";
paths.minCss = paths.webroot + "css/**/*.min.css";
paths.concatGzDest = paths.webroot + "bundle/gz";
paths.concatJsDest = paths.webroot + "bundle/js/site.min.js";
paths.concatCssDest = paths.webroot + "bundle/css/site.min.css";

gulp.task("clean:js", done => rimraf(paths.concatJsDest, done));
gulp.task("clean:gz", done => rimraf(paths.concatGzDest, done));
gulp.task("clean:css", done => rimraf(paths.concatCssDest, done));
gulp.task("clean", gulp.series(["clean:js", "clean:css", "clean:gz"]));

gulp.task('gzip:gz', function () {
    return gulp.src(paths.js)
        .pipe(gzip())
        .pipe(gulp.dest(paths.concatGzDest));
});

gulp.task("min:js", () => {
    return gulp.src([paths.js, "!" + paths.minJs], { base: "." })
        .pipe(concat(paths.concatJsDest))
        .pipe(uglify())
        .pipe(gulp.dest("."));
});

gulp.task("min:css", () => {
    return gulp.src([paths.css, "!" + paths.minCss])
        .pipe(concat(paths.concatCssDest))
        .pipe(cssmin())
        .pipe(gulp.dest("."));
});

gulp.task("min", gulp.series(["min:js", "min:css", "gzip:gz"]));

// A 'default' task is required by Gulp v4
gulp.task("default", gulp.series(["min"]));
